import { history } from '@umijs/max';
import {
  Divider,
  FormItem,
  FormStep,
  FormSteps,
  FreeForm,
  LocaleProvider,
  RadioGroup,
  Typography,
} from 'iglooform';
import { CheckOutlined, CrossOutlined } from 'iglooicon';
import styles from './index.less';

export default () => {
  const previewFormater = (value: any) => {
    switch (!!value) {
      case true:
        return <CheckOutlined style={{ color: 'green', fontSize: 24 }} />;
      case false:
        return <CrossOutlined style={{ color: 'red', fontSize: 24 }} />;
    }
  };

  const getScope = (value: Record<string, any>) => {
    let scope = 0;
    Object.entries(value).map(([k, v]) => {
      scope += Number(v);
    });
    return scope;
  };

  return (
    <LocaleProvider currentLang="zh-CN">
      <div className={styles.formContainer}>
        <FreeForm
          locales={{
            validateMessages: {
              required: '该选项必选',
            },
          }}
          onSubmit={(value) => {
            sessionStorage.setItem('value', JSON.stringify(value));

            const { ADL, IADL, CCI, age } = value;

            sessionStorage.setItem(
              'scope',
              JSON.stringify({
                ADL: getScope(ADL),
                IADL: getScope(IADL),
                CCI: getScope(CCI),
                age: getScope(age),
              }),
            );

            history.push('/result');
          }}
        >
          <FormSteps>
            <FormStep
              name="ADL"
              label={<Typography level="h4">日常基本活动量</Typography>}
            >
              <FormItem
                label="完全自己洗澡或仅在浴池内需要帮助"
                name="a1"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="自己拿，穿衣服，可以辅助穿鞋"
                name="a2"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="自己去厕所，穿脱衣和清洁厕具"
                name="a3"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="自己起床，躺下和坐下，站起，允许用辅助工具"
                name="a4"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="排泄：大小便完全自理"
                name="a5"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="自己进食"
                name="a6"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
            </FormStep>

            <Divider></Divider>

            <FormStep
              name="IADL"
              label={
                <Typography level="h4">
                  Lawton和Brody的工具性日常生活活动能力
                </Typography>
              }
              previewFormater={previewFormater}
            >
              <FormItem
                label="能否自己打电话"
                name="b1"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="能否购物"
                name="b2"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="能否烧菜做饭"
                name="b3"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="能否家务"
                name="b4"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="能否洗衣服"
                name="b5"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="交通模式：利用公共交通自由出行，或自己开车"
                name="b6"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="能否自行服药"
                name="b7"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="能否处理财务"
                name="b8"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: true,
                    },
                    {
                      label: '否',
                      value: false,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
            </FormStep>

            <Divider></Divider>

            <FormStep
              name="CCI"
              label={<Typography level="h4">Charlson共存病指数</Typography>}
              previewFormater={previewFormater}
            >
              <FormItem
                label="心梗"
                name="c1"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="充血性心力衰竭"
                name="c2"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="周围血管病变"
                name="c3"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="脑血管病"
                name="c4"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="痴呆"
                name="c5"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="慢性肺病"
                name="c6"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="结缔组织病"
                name="c7"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="溃疡"
                name="c8"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="轻度肝病"
                name="c9"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="糖尿病"
                name="c10"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 1,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="糖尿病伴靼器官损伤"
                name="c11"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 2,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="中重度肾功能不全"
                name="c12"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 2,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="非转移性实体肿瘤"
                name="c13"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 2,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="白血病"
                name="c14"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 2,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="淋巴瘤"
                name="c15"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 2,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="转移的实体肿瘤"
                name="c16"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 6,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="中重度肝病"
                name="c17"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 3,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
              <FormItem
                label="艾滋"
                name="c18"
                previewFormater={previewFormater}
              >
                <RadioGroup
                  options={[
                    {
                      label: '是',
                      value: 6,
                    },
                    {
                      label: '否',
                      value: 0,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
            </FormStep>

            <Divider></Divider>
            <FormStep
              label={<Typography level="h4">年纪范围</Typography>}
              name="age"
            >
              <FormItem name="ageRange">
                <RadioGroup
                  options={[
                    {
                      label: '小于等于75',
                      value: 0,
                    },
                    {
                      label: '76到80',
                      value: 1,
                    },
                    {
                      label: '大于80',
                      value: 2,
                    },
                  ]}
                ></RadioGroup>
              </FormItem>
            </FormStep>
          </FormSteps>
        </FreeForm>
      </div>
    </LocaleProvider>
  );
};
