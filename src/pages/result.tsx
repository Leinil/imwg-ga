import { history } from '@umijs/max';
import type { TableProps } from 'antd';
import { Table } from 'antd';
import { Button, Typography } from 'iglooform';

import styles from './result.css';

const Result = () => {
  let fin = 0;
  let finLab;
  let finCss;
  let fintype;
  const gaScopeMap: any = {};

  const { age, ADL, IADL, CCI } = JSON.parse(
    sessionStorage.getItem('scope') || '{}',
  );

  fin += age;

  if (ADL <= 4) {
    fin += 1;
    gaScopeMap['ADL'] = 1;
  } else {
    gaScopeMap['ADL'] = 0;
  }

  if (IADL <= 5) {
    fin += 1;
    gaScopeMap['IADL'] = 1;
  } else {
    gaScopeMap['IADL'] = 0;
  }

  if (CCI >= 2) {
    fin += 1;
    gaScopeMap['CCI'] = 1;
  } else {
    gaScopeMap['CCI'] = 0;
  }

  switch (fin) {
    case 0:
      finLab = '健康';
      finCss = 'rgb(137,217,97)';
      fintype = 'Fit';
      break;
    case 1:
      finLab = '中等';
      finCss = 'rgb(192, 159, 91)';
      fintype = 'intermediate-fitness';
      break;
    default:
      finLab = '虚弱';
      finCss = 'rgb(158,0,0)';
      fintype = 'Frailty';
  }

  const columns: TableProps<any>['columns'] = [
    {
      title: '项目',
      dataIndex: 'project',
      onCell: (_, rowIndex) => {
        switch (rowIndex) {
          case 0:
            return { rowSpan: 3 };
          case 3:
          case 5:
          case 7:
            return { rowSpan: 2 };
          case 9:
            return { rowSpan: 3 };
          default:
            return { rowSpan: 0 };
        }
      },
    },
    {
      title: '对应范围',
      dataIndex: 'defineRange',
      align: 'center',
      render: (text) => <a>{text}</a>,
    },
    {
      title: 'GA得分 / 健康评价',
      dataIndex: 'scope',
      align: 'center',
    },
  ];

  const data: any[] = [
    {
      project: '年纪',
      defineRange: '≤75',
      scope: 0,
    },
    {
      project: '年纪',
      defineRange: '76-80',
      scope: 1,
    },
    {
      project: '年纪',
      defineRange: '>80',
      scope: 2,
    },

    {
      project: 'ADL评分',
      defineRange: '>4',
      scope: 0,
    },
    {
      project: 'ADL评分',
      defineRange: '≤4',
      scope: 1,
    },
    {
      project: 'IADL评分',
      defineRange: '>5',
      scope: 0,
    },
    {
      project: 'IADL评分',
      defineRange: '≤5',
      scope: 1,
    },

    {
      project: 'CCI评分',
      defineRange: '0-1',
      scope: 0,
    },
    {
      project: 'CCI评分',
      defineRange: ' ≥2',
      scope: 1,
    },
    {
      project: 'IMWG-GA评分总分',
      scope: '健康',
      defineRange: 0,
    },
    {
      project: 'IMWG-GA评分总分',
      scope: ' 中等',
      defineRange: 1,
    },
    {
      project: 'IMWG-GA评分总分',
      scope: ' 虚弱',
      defineRange: '≥2',
    },
  ];

  const columnsScope = [
    {
      title: '项目',
      dataIndex: 'projectName',
    },
    {
      title: '得分 / 选择范围',
      dataIndex: 'scope',
      render: (value: any, record: any) => {
        if (record.project === 'age') {
          switch (value) {
            case 0:
              return '≤75';
            case 1:
              return '76-80';
            case 2:
              return '>80';
          }
        }
        return value;
      },
    },
    {
      title: 'GA得分/健康评价',
      dataIndex: 'ga',
      render: (value: any, record: any) => {
        if (record.project === 'age') {
          return age;
        }
        if (record.project === 'ga') {
          return (
            <>
              <div>
                <Typography level="h4" style={{ fontWeight: 800 }}>
                  评价：
                </Typography>
                <Typography
                  level="h4"
                  style={{ color: finCss, fontWeight: 800 }}
                >
                  {finLab} / {fintype}
                </Typography>
              </div>
            </>
          );
        }
        return gaScopeMap[record.project];
      },
    },
  ];

  const dataScope = [
    {
      projectName: '年纪',
      scope: age,
      project: 'age',
    },
    {
      projectName: 'ADL评分',
      scope: ADL,
      project: 'ADL',
    },
    {
      projectName: 'IADL评分',
      scope: IADL,
      project: 'IADL',
    },
    {
      projectName: 'CCI评分',
      scope: CCI,
      project: 'CCI',
    },

    {
      projectName: 'IMWG-GA评分总分',
      scope: fin,
      project: 'ga',
    },
  ];

  return (
    <div className="flex itens-center justify-center flex-col">
      <div className={styles.topMessage}>
        <div>
          <Typography level="body2">
            老年评分系统（Geriatric
            Assessment，GA）由国际骨髓瘤工作小组IMWG于2015年正式提出，包含年龄、日常基本活动量表（Activities
            of Daily living，ADL）、
            Lawton和Brody的工具性日常生活活动能力评估(Instrumental Activities of
            Daily living，IADL)、Charlson共存病指数(Charlson Comorbidity
            Index，CCI)四项内容。
          </Typography>
        </div>
        <div>
          <Typography level="body2">
            是聚焦身体、功能和社会心理方面来客观地评价老年人的健康状态的工具，已广泛用于评估老年恶性肿瘤患者的综合状态。
            四张量表加起来的总分即为GA的总分，其中0分是Fit（健康），1分是intermediate-fitness（中等虚弱），如果总分≥2分即为Frailty（虚弱）
          </Typography>
        </div>
      </div>

      <Typography level="h3a" style={{ fontWeight: 600, marginBottom: 12 }}>
        IMWG-GA评分总分表:
      </Typography>
      <Table
        columns={columns}
        dataSource={data.map((i, index) => ({ ...i, key: index }))}
        bordered
        className={styles.table}
        rowKey={'key'}
        pagination={{
          pageSize: 20,
          current: 1,
          hideOnSinglePage: true,
        }}
      />
      <Typography level="h3a" style={{ fontWeight: 600, marginBottom: 12 }}>
        您的得分:
      </Typography>
      <Table
        columns={columnsScope}
        dataSource={dataScope.map((i, index) => ({ ...i, key: index }))}
        bordered
        className={styles.table}
        rowKey={'key'}
        pagination={{
          pageSize: 20,
          current: 1,
          hideOnSinglePage: true,
        }}
      />
      <div className="flex items-center justify-center mt-6">
        <Button type="primary" size="middle" onClick={() => history.push('/')}>
          重新测试
        </Button>
      </div>
    </div>
  );
};

export default Result;
