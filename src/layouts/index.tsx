import { Typography } from 'iglooform';
import { Outlet } from 'umi';

export default function Page() {
  return (
    <div>
      <div
        className={'py-2 w-full flex items-center justify-center'}
        style={{background: 'rgb(198,0,22)'}}
      >
        <Typography level={'h3a'} style={{ color: 'white' }}>
          IMWG-GA评分系统
        </Typography>
      </div>
      <div className="flex justify-center bg-white p-4">
        <div className="max-w-[800px] w-full ">
          <Outlet />
        </div>
      </div>
    </div>
  );
}
